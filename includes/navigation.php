<a class="min-menu  <?php if($_SERVER["REQUEST_URI"] === "/" or $_SERVER["REQUEST_URI"] === "/index.php"){ echo "" ;} else {echo "hidden-lg hidden-md";  };?>" onclick="showmenu();">
    <span class="bar"></span>
    <span class="bar"></span>
    <span class="bar"></span>
</a>
<div id="navigation-min" style="padding: 10px; border-bottom: 2px solid #19bd9a;">
    <div class="container text-center" style="padding-top: 30px;">
    <ul class="nav nav-pills nav-stacked ">
        <li id="menu-index" class="<?php if($_SERVER["REQUEST_URI"] =='/index.php'){ echo 'active'; };?>" >
            <a href="/index.php" >
                Главная
            </a>
        </li>
        <li id="menu-about" class="<?php if($_SERVER["REQUEST_URI"] =='/pages/about.php'){ echo 'active'; };?>" >
            <a href="/pages/about.php" >
                Обо мне
            </a>
        </li>
        <li id="menu-wedding" class="<?php if($_SERVER["REQUEST_URI"] =='/pages/wedding.php'){ echo 'active'; };?>">
            <a href="/pages/wedding.php" >
                Свадебный ужин
            </a>
        </li>
        <li id="menu-corporate" class="<?php if($_SERVER["REQUEST_URI"] =='/pages/corporate.php'){ echo 'active'; };?>">
            <a href="/pages/corporate.php">
                Корпоративный вечер
            </a>
        </li>
        <li id="menu-reviews" class="<?php if($_SERVER["REQUEST_URI"] =='/pages/reviews.php'){ echo 'active'; };?>">
            <a href="/pages/reviews.php">
                Отзывы и рекомендации
            </a>
        </li>
        <li id="menu-contact" class="<?php if($_SERVER["REQUEST_URI"] =='/pages/contact.php'){ echo 'active'; };?>">
            <a href="/pages/contact.php">
                Контакты
            </a>
        </li>
    </ul>
    <ul class="list-inline text-center social" style="margin-top: 20px; position: relative;">
        <!--<li><a href="#"><img class="blackeffect" src="img/facebook.png" alt=""></a></li>
         <li><a href="#"><img class="blackeffect" src="img/twitter.png" alt=""></a></li>-->
        <li><a href="https://www.instagram.com/kirill_ulyanychev/?hl=ru" target="_blank"><img class="blackeffect" src="../img/instagramm.png" alt=""></a></li>
        <li><a href="https://vk.com/id39347847" target="_blank"><img class="blackeffect" src="../img/vimeo.png" width="28px" height="28px"  alt=""></a></li>
    </ul>
    </div>
</div>
<div class="navigation hidden-xs hidden-sm <?php if($_SERVER["REQUEST_URI"] === "/" or $_SERVER["REQUEST_URI"] === "/index.php" ){ echo "hidden" ;} else {echo "col-md-3"; };?> ">

    <a href="/"><h1 class="text-center" >Кирилл Ульянычев</h1></a>
    <p class="text-center" style="margin-top: 40px;"><img  src="../img/hr.png"  alt=""></p>
  <ul class="nav nav-pills nav-stacked ">
      <li id="menu-about" class="<?php if($_SERVER["REQUEST_URI"] =='/pages/about.php'){ echo 'active'; };?>" >
          <a href="/pages/about.php" >
    Обо мне
</a>
      </li>
      <li id="menu-wedding" class="<?php if($_SERVER["REQUEST_URI"] =='/pages/wedding.php'){ echo 'active'; };?>">
          <a href="/pages/wedding.php" >
    Свадебный ужин
</a>
      </li>
      <li id="menu-corporate" class="<?php if($_SERVER["REQUEST_URI"] =='/pages/corporate.php'){ echo 'active'; };?>">
          <a href="/pages/corporate.php">
    Корпоративный вечер
</a>
      </li>
      <li id="menu-reviews" class="<?php if($_SERVER["REQUEST_URI"] =='/pages/reviews.php'){ echo 'active'; };?>">
          <a href="/pages/reviews.php">
    Отзывы и рекомендации
</a>
      </li>
      <li id="menu-contact" class="<?php if($_SERVER["REQUEST_URI"] =='/pages/contact.php'){ echo 'active'; };?>">
          <a href="/pages/contact.php">
    Контакты
          </a>
      </li>
  </ul>
        <ul class="list-inline text-center social">
           <!--<li><a href="#"><img class="blackeffect" src="img/facebook.png" alt=""></a></li>
            <li><a href="#"><img class="blackeffect" src="img/twitter.png" alt=""></a></li>-->
            <li><a href="https://www.instagram.com/kirill_ulyanychev/?hl=ru" target="_blank"><img class="blackeffect" src="../img/instagramm.png" alt=""></a></li>
            <li><a href="https://vk.com/id39347847" target="_blank"><img class="blackeffect" src="../img/vimeo.png" width="28px" height="28px"  alt=""></a></li>
        </ul>
</div>
