<?php
include "../includes/header.php";
include "../includes/navigation.php";
?>

<div id="about" class="col-xs-12 col-md-8 col-md-offset-3">
    <div class="container-fluid">
        <h2 class="text-center" style="padding-bottom: 40px; ">Обо мне</h2>
        <div class="row">
        <div class="col-xs-12 col-sm-5">
            <img src="../img/about.jpg" alt="" width="100%">
        </div>
        <div class="col-xs-12 col-sm-7">
            <p>В Event сфере с 2012 года</p>
            <p> Работаю на качество, а не на количество. К каждому клиенту подхожу индивидуально, без шаблонов, панибратства!
            </p>
            <p>Связываю между собой все элементы праздника.</p>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-12"><h1 class="text-left" style=" padding-top: 30px;">Мои клиенты</h1></div>
        <div class="col-xs-3 col-sm-2"><img src="../img/clients/post_rf.jpg" alt="" width="100%"></div>
        <div class="col-xs-3 col-sm-2"><img src="../img/clients/MTS_logo.png" alt="" width="100%" style="margin-top: 10px;"></div>
        <div class="col-xs-3 col-sm-2"><img src="../img/clients/kfc.jpg" alt="" width="75%"></div>
        <div class="col-xs-3 col-sm-2"><img src="../img/clients/amperial.jpg" alt="" width="75%"></div>
        <div class="hidden-xs col-sm-2"><img src="../img/clients/mcota.jpg" alt="" width="100%"></div>
        <div class="hidden-xs col-sm-2"><img src="../img/clients/magnit.jpg" alt="" width="100%"></div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-xs-3 col-sm-2"><img src="../img/clients/beeline.jpg" alt="" width="75%"></div>
            <div class="col-xs-3 col-sm-2"><img src="../img/clients/5terka.jpg" alt="" width="75%"></div>
            <div class="col-xs-3 col-sm-2"><img src="../img/clients/74Wq2IF1JT4.jpg" alt="" width="75%"></div>
        </div>
    </div>
</div>

<?php
include "../includes/footer.php";
?>