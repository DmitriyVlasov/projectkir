<?php
include "../includes/header.php";
include "../includes/navigation.php";
?>
        <div id="corporate" class="col-xs-12 col-md-8 col-md-offset-3">
            <div class="container-fluid">
                <h2 class="text-center" style="padding-bottom: 30px; ">Корпоративный вечер</h2>
                <div class="row">
                <div class="col-xs-12 col-sm-4"><img src="../img/corporate-1.jpg" alt="" width="100%" height="auto"></div>
                <div class="col-xs-12 col-sm-8">
                    <p style="margin-top: 15px;">Вечер, где компания должна отдохнуть по полной, забыть о работе, погрузиться в атмосферу праздника с головой!</p>
                    <p>Работаю, учитывая пожелания заказчика, главное для меня : что бы Вы остались довольны и получили удовольствие от мероприятия на все 100%</p>
                    <p style="padding-bottom: 30px;">Я, очень компромиссный человек, со мной всегда можно договориться, вам остается лишь попробовать это сделать!</p>
                </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h4 class="text-left" style="padding-bottom: 10px; padding-top: 40px; ">Свадебная выставка « park house “</h4>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group6" href="../img/parkhouse/D0iFx_muhS8.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/parkhouse/D0iFx_muhS8.jpg"/></a></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group6" href="../img/parkhouse/wW1IfE7O4kY.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/parkhouse/wW1IfE7O4kY.jpg"/></a></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                    <h4 class="text-left" style="padding-bottom: 10px; padding-top: 40px; ">Показ одежды irina_karyagina _desing</h4>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group3" href="../img/bg_home.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/bg_home.jpg"/></a></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group3" href="../img/wedding-2.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/wedding-2.jpg"/></a></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group3" href="../img/podium/podium3.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/podium/podium3.jpg"/></a></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h4 class="text-left" style="padding-bottom: 10px; padding-top: 40px; ">Почта России</h4>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group4" href="../img/corporate_post/post1.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_post/post1.jpg"/></a></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group4" href="../img/corporate_post/post2.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_post/post2.jpg"/></a></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group4" href="../img/corporate_post/post3.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_post/post3.jpg"/></a></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group4" href="../img/corporate_post/post4.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_post/post4.jpg"/></a></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group4" href="../img/corporate_post/post5.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_post/post5.jpg"/></a></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group4" href="../img/corporate_post/post6.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_post/post6.jpg"/></a></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h4 class="text-left" style="padding-bottom: 10px; padding-top: 40px; ">Компания МСота</h4>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group5" href="../img/corporate_mcota/mcota1.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_mcota/mcota1.jpg"/></a></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group5" href="../img/corporate_mcota/mcota2.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_mcota/mcota2.jpg"/></a></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group5" href="../img/corporate_mcota/mcota3.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_mcota/mcota3.jpg"/></a></div>
                        <div class="row"></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group5" href="../img/corporate_mcota/mcota4.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_mcota/mcota4.jpg"/></a></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group5" href="../img/corporate_mcota/mcota7.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_mcota/mcota7.jpg"/></a></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group5" href="../img/corporate_mcota/mcota9.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_mcota/mcota9.jpg"/></a></div>
                        <div class="row"></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group5" href="../img/corporate_mcota/mcota5.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_mcota/mcota5.jpg"/></a></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group5" href="../img/corporate_mcota/mcota6.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_mcota/mcota6.jpg"/></a></div>
                        <div class="col-xs-4" style="margin-bottom: 10px;"><a rel="example_group5" href="../img/corporate_mcota/mcota10.jpg"><img class="corporate-image" alt="" width="100%" height="auto" src="../img/corporate_mcota/mcota10.jpg"/></a></div>
                    </div>
                </div>
            </div>
        </div>

<?php
include "../includes/footer.php";
?>