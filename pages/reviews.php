<?php
include "../includes/header.php";
include "../includes/navigation.php";
?>
        <div id="rewiews" class="container-fluid  col-xs-12 col-md-8 col-md-offset-3">
            <h2 class="text-center" style="padding-bottom: 40px; ">Отзывы и рекомендации</h2>
            <p style="padding-bottom: 50px; text-align: center; border-bottom: 1px solid #eee;">Больше всего радует, что после моей работы я получаю тёплые и приятные слова в свой адрес!</p>
                        <div class="row" style="padding-bottom: 50px; margin-top:30px; border-bottom: 2px solid #eee;">
                            <div class="col-xs-4 col-sm-2" style="margin-bottom: 30px;">
                                <img class="avatar" src="../img/reviews-1.jpg" alt="">
                            </div>
                            <div class="col-xs-12 col-sm-10">
                                <p >Кирилл, #АртСтудияДобрыйДень выражает тебе благодарность за проведенный праздник День защиты детей<img src="../img/icon-biceps.png"> </p>
                                <p style="padding-bottom:50px;"> Пунктуальность и четкость исполнения поставленных задач была на самом высоком уровне:) с тобой приятно и ценно сотрудничать! До новых встреч <img src="../img/icon-hans.png"></p>
                                <p  style="font-weight:bold; color:#19bd9a; line-height: 1;">Михаил Северьян</p>
                            </div>
                        </div>
                        <div class="row " style="padding-bottom: 50px; margin-top: 30px; border-bottom: 2px solid #eee;">
                            <div class="col-xs-4 hidden-sm hidden-md hidden-lg" style="margin-bottom: 30px;">
                                <img class="avatar" src="../img/reviews-2.jpg" alt="">
                            </div>
                            <div class="col-xs-12 col-sm-10">
                                <p >Кирилл, спасибо большое тебе от агентства #АнимаМос!
                                    Ты Ведущий, который моментально вливается, с легкостью импровизирует и самое важное, что ты чувствуешь публику и держишь внимание</p>

                                <p style="padding-bottom:50px;">  Работать легко и приятно)</p>
                                <p  style="font-weight:bold; color:#19bd9a; line-height: 1;">Елена Абаева</p>
                            </div>
                            <div class="hidden-xs col-sm-2" style="margin-bottom: 30px;">
                                <img class="avatar" src="../img/reviews-2.jpg" alt="">
                            </div>
                        </div>
                        <div class="row" style="padding-bottom: 50px; margin-top: 30px; border-bottom: 2px solid #eee;">
                            <div class="col-xs-4 col-sm-2" style="margin-bottom: 30px;">
                                <img class="avatar" src="../img/reviews-3.jpg" alt="">
                            </div>
                            <div class="col-xs-12 col-sm-10">
                                <p style="padding-bottom:50px;">Кирилл, огромное спасибо за то, что откликнулся в короткие сроки; за то, что всегда был на связи; за то что не подвел (работали вместе вперые)!!; за то что приехал во время; за то что создал неповторимую атмосферу праздника; за то что был профессионален, тактичен, внимателен; за аккуратный образ и отличный гримм! Клиенты, компании "Уралхим", остались очень довольны !</p>
                                <p  style="font-weight:bold; color:#19bd9a; line-height: 1;">Ольга Сорокина</p>
                            </div>
                        </div>
                </div>
<?php
include "../includes/footer.php";
?>