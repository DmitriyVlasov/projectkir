<?php
include "../includes/header.php";
include "../includes/navigation.php";
?>

    <div id="wedding" class="col-xs-12 col-md-8 col-md-offset-3">
        <div class="container-fluid">
            <div class="row" style="border-bottom: 2px solid #19bd9a;">
            <h2 class="text-center" style="padding-bottom: 40px;  ">Свадебный ужин</h2>
            <div class="col-xs-12 text-center" style="padding-bottom: 50px;">
                <div class="col-xs-12 col-sm-4"><img style="cursor: default" src="../img/wedding/n0DhswoGas8.jpg"/></div>
                <div class="col-xs-12 col-sm-8">
                <p>Самое долгожданное событие в жизни каждой девушки и поэтому хочется, что бы оно прошло на высшем уровне! Все хотят чего то нового, необычного, я ,как человек креативный помогу вам в этом! Мы сделаем с вами вместе праздник, который запомниться на всю жизнь с лучшей стороны, чего бы мне это не стоило!
                    Клиент всегда прав, клиент должен быть доволен! Доволен клиент, значит я все правильно сделал!</p>
                </div>
            </div>
            </div>
            <div class="row wedding_row" style="padding: 30px; margin-bottom: 30px; border-bottom: 2px solid #19bd9a;">
                <div class="row">
                <div class="col-xs-4"><a rel="example_group" href="../img/weddin-1.jpg"><img alt="" src="../img/weddin-1.jpg"/></a></div>
                <div class="col-xs-4"><a rel="example_group" href="../img/weddin-3.jpg"><img alt="" src="../img/weddin-3.jpg"/></a></div>
                <div class="col-sm-4  col-xs-4"><a rel="example_group" href="../img/wedding/w1.jpg"><img alt="" src="../img/wedding/w1.jpg"/></a></div>
                </div>
                <div class="row">
                <div class="col-xs-4"><a rel="example_group" href="../img/wedding/w2.jpg"><img alt="" src="../img/wedding/w2.jpg"/></a></div>
                <div class="col-xs-4"><a rel="example_group" href="../img/wedding/w3.jpg"><img alt="" src="../img/wedding/w3.jpg"/></a></div>
                    <div class="col-xs-4"><a rel="example_group" href="../img/wedding/w4.jpg"><img alt="" src="../img/wedding/w4.jpg"/></a></div>
                </div>
            </div>
        </div>
    </div>
<?php
include "../includes/footer.php";
?>