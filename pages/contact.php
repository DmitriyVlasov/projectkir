<?php
include "../includes/header.php";
include "../includes/navigation.php";
?>


<div id="contact" class="col-xs-12 col-md-10 col-md-offset-2">
    <div class="container-fluid">
        <h2 class="text-center" style="padding-bottom: 50px; ">Контакты</h2>
        <div class="col-xs-12 col-md-5 col-md-offset-1">
            <p>Привет! Меня зовут Ульянычев Кирилл, я веду мероприятия различного характера!<br>
                Веду легко и непринужденно, а главное с максимальной отдачей !</p>
            <p style="padding-top: 35px;">Живу в Москве, доступен по всему миру.</p>
            <p>Мой телефон:</p>
            <p>+7 (915) 487-90-71</p>
            <p>Электронная почта:</p>
            <p>Ulyanychevkirill@mail.ru</p>
        </div>
    </div>
</div>

<?php
include "../includes/footer.php";
?>
